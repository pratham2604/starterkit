import FIREBASE_CONFIG from './firebase.env';
import { TEMPLATE_TYPES } from './templates/index';

const toastConfig = {
  position: "top-right",
  autoClose: 5000,
  hideProgressBar: true,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true,
}
const APP_TITLE = 'Starter Kit';

export default {
  FIREBASE_CONFIG,
  TOAST_CONFIG: toastConfig,
  APP_TITLE,
  DEFAULT_TEMPLATE: TEMPLATE_TYPES.GUEST,
};