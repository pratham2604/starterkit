import GuestTemplate from './guestTemplate';
import AuthNavbarTemplate from './authNavbarTemplate';
import AuthSidebarTemplate from './authSidebarTemplate/authSidebarTemplate';

export const TEMPLATE = {
  GUEST: GuestTemplate,
  AUTH_NAVBAR: AuthNavbarTemplate,
  AUTH_SIDEBAR: AuthSidebarTemplate,
};

export const TEMPLATE_TYPES = {
  GUEST: 'GUEST',
  AUTH_NAVBAR: 'AUTH_NAVBAR',
  AUTH_SIDEBAR: 'AUTH_SIDEBAR',
};