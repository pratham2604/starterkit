import React, { Fragment, Component } from 'react';
import { Icon, Menu, Sidebar, Container } from 'semantic-ui-react';
import { Helmet } from 'react-helmet';
import SideMenu from '../../../components/sideMenu/sideMenu';

export default class extends Component {
  state = {
    showMenu: true,
    activePath: '',
  }

  toggleMenu = () => {
    this.setState({
      showMenu: !this.state.showMenu,
    });
  }

  onMenuChange = (activePath) => {
    this.setState({
      activePath,
    });
  }

  render() {
    const { children, pageTitle, signOut } = this.props;
    const { showMenu, activePath } = this.state;

    return (
      <Fragment>
        <Helmet>
          <title>{pageTitle}</title>
        </Helmet>
        <Sidebar.Pushable className="auth-sidebar-template">
          <Sidebar as={Menu} animation='push' inverted vertical visible={showMenu}>
            <SideMenu onMenuChange={this.onMenuChange} />
          </Sidebar>
          <Sidebar.Pusher className="template-container">
            <Menu fixed='top' inverted>
              <Container>
                <Menu.Item as='a' header onClick={this.toggleMenu}>
                  <Icon name="bars" />
                </Menu.Item>
                <Menu.Item as='a' header>
                  {activePath}
                </Menu.Item>
                <Menu.Item as='a' position='right' className="sign-out-button" onClick={signOut}>Sign Out</Menu.Item>
              </Container>
            </Menu>
            <Container fluid className="module-container">
              {children}
            </Container>
          </Sidebar.Pusher>
        </Sidebar.Pushable>
      </Fragment>
    );
  }
}