import { Firestore, Firebase } from '../lib/firebase';
import * as TYPES from '../web/constants/types';
import { toast } from 'react-toastify';

export const fetchAuth = () => {
  return (dispatch) => {
    dispatch({type: TYPES.FETCH_AUTH_REQUEST});
    return Firebase.auth().onAuthStateChanged(user => {
      console.log('auth state has changed', user)
      if (user) {
        const { email, emailVerified, uid, refreshToken, displayName, photoURL } = user;
        const data = Object.assign({}, { email, emailVerified, uid, displayName, photoURL });
        dispatch({
          type: TYPES.FETCH_AUTH_SUCCESS,
          data,
          token: refreshToken,
        });
      } else {
        const message = 'User auth not present';
        dispatch({type: TYPES.FETCH_AUTH_FAILURE, message});
      }
    });
  }
}

export const signUp = (data) => {
  const { email, password } = data;
  return (dispatch) => {
    dispatch({type: TYPES.SIGNUP_REQUEST});
    Firebase.auth().createUserWithEmailAndPassword(email, password).then(response => {
      const { user } = response;
      if (user) {
        const { email, emailVerified, uid, displayName, photoURL } = user;
        const data = Object.assign({}, { email, emailVerified, uid, displayName, photoURL });
        Firestore.collection("users").doc(data.uid).set(data).catch((error) => {
          toast.error(error);
        });
      } else {
        const message = 'Unable to register account';
        toast.error(message);
        dispatch({type: TYPES.SIGNUP_FAILURE});
      }
    }).catch(err => {
      const { message = 'Unable to register account' } = err;
      toast.error(message);
      dispatch({type: TYPES.SIGNUP_FAILURE});
    });
  }
}

export const login = (data) => {
  const { email, password } = data;
  return (dispatch) => {
    dispatch({type: TYPES.LOGIN_REQUEST});
    Firebase.auth().signInWithEmailAndPassword(email, password).catch(err => {
      const { message = 'Unable to login' } = err;
      toast.error(message);
      dispatch({type: TYPES.LOGIN_FAILURE});
    });
  }
}

export const signOut = () => {
  return (dispatch) => {
    dispatch({type: TYPES.SIGNOUT_REQUEST});
    return Firebase.auth().signOut();
  }
}