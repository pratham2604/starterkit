import Form from './form/form';
import Input from './input/input';
import SubmitButton from './submitButton/submitButton';

export default {
  Form,
  Input,
  SubmitButton,
};
