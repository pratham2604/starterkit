import React, { Component, Fragment } from 'react';
import { Menu, Header } from 'semantic-ui-react';
import { Link, withRouter } from 'react-router-dom';
import ImageHolder from '../UI/imageHolder/imageHolder';
import './sideMenu.scss';

const routes = [{
  title: 'Home',
  to: '/',
  id: 'home'
}, {
  title: 'Menu',
  to: '/menu',
  id: 'menu',
}, {
  title: 'Users',
  to: '/users',
  id: 'user',
}];

class SideMenu extends Component {
  componentDidMount() {
    const { location } = this.props;
    const { pathname } = location;
    const currentRoute = routes.find(route => route.to === pathname) || {};
    this.props.onMenuChange(currentRoute.title || '');
  }

  onMenuChange = (path) => {
    this.props.onMenuChange(path);
  }

  render() {
    const { location } = this.props;
    const { pathname } = location;

    return (
      <Fragment>
        <Menu.Item className="side-menu-header-buffer">
        </Menu.Item>
        <Menu.Item as='a' className="side-menu-info-container">
          <ImageHolder size='medium' rounded src="" />
          <Header as='h3' className="title">Title</Header>
          <Header as='h4' className="description">Description</Header>
        </Menu.Item>
        {routes.filter(route => {
          // Route filter condition
          return true;
        }).map((route, index) => {
          const { to, title } = route;
          return (
            <Menu.Item as={Link} to={to} key={index} active={pathname === to} onClick={this.onMenuChange.bind(this, title)}>
              {title}
            </Menu.Item>
          )
        })}
      </Fragment>
    )
  }
}

export default withRouter(SideMenu);