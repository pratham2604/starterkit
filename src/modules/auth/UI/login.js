import React, { Component } from 'react';
import { Grid, Header, Message, Segment } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import FC from '../../../components/formsy/index';

class LoginForm extends Component {
  state = {
    canSubmit: false,
  }

  validations = {
    email: [{
      validator: 'isRequired',
    }, {
      validator: 'isEmail',
    }], 
    password: [{
      validator: 'isRequired',
    }]
  }

  enableButton = () => {
    this.setState({
      canSubmit: true,
    });
  }

  disableButton = () => {
    this.setState({
      canSubmit: false,
    });
  }

  onSubmit = (model) => {
    const { onLogin } = this.props;
    onLogin(model);
  }

  render() {
    const { canSubmit } = this.state;
    const { loading } = this.props;

    return (
      <Grid textAlign='center' className="auth-form" verticalAlign='middle'>
        <Grid.Column width={6}>
          <Header as='h2' color='teal' textAlign='center'>
            Log-in to your account
          </Header>
            <Segment stacked>
              <FC.Form onValid={this.enableButton} onInvalid={this.disableButton} onSubmit={this.onSubmit} validations={this.validations}>
                <FC.Input fluid icon='mail' iconPosition='left' placeholder='E-mail address' name="email" formValidations={this.validations.email} required/>
                <FC.Input fluid icon='lock' iconPosition='left' placeholder='Password' name="password" type="password" formValidations={this.validations.password} required/>
                <FC.SubmitButton loading={loading} disabled={!canSubmit} color='teal' fluid size='large' type="submit">Login</FC.SubmitButton>
              </FC.Form>
            </Segment>
          <Message>
            New to us? <Link to="/signup">Sign Up</Link>
          </Message>
        </Grid.Column>
      </Grid>
    )
  }
}

export default LoginForm;